Source: libzypp
Section: libs
Maintainer: RPM packaging team <team+pkg-rpm@tracker.debian.org>
Uploaders: Luca Boccassi <bluca@debian.org>,
           Mike Gabriel <sunweaver@debian.org>,
Priority: optional
Standards-Version: 4.7.0
Build-Depends: asciidoctor,
               debhelper-compat (= 13),
               dh-exec,
               dpkg-dev (>= 1.16.1.1~),
               cmake,
               pkgconf,
               librpm-dev (>= 4.4),
               rpm,
               libboost-dev,
               libboost-program-options-dev,
               libboost-test-dev,
               libboost-thread-dev,
               libfcgi-dev,
               libglib2.0-dev,
               libgpgme-dev,
               libsigc++-2.0-dev,
               libssl-dev,
               libyaml-cpp-dev,
               doxygen <!nodoc>,
               ruby,
               libcurl4-openssl-dev (>= 7.19.4),
               libudev-dev,
               libxml2-dev,
               libsolv-dev (>= 0.7.24),
               libsolv-tools,
               libexpat-dev,
               libpopt-dev,
               libproxy-dev,
               graphviz <!nodoc>,
               gnupg2,
               dejagnu,
               nginx,
               vsftpd,
Rules-Requires-Root: no
Homepage: https://github.com/openSUSE/libzypp
Vcs-Git: https://salsa.debian.org/pkg-rpm-team/libzypp.git
Vcs-Browser: https://salsa.debian.org/pkg-rpm-team/libzypp

Package: libzypp1735
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 rpm,
 gnupg | gnupg2,
 libsolv-tools,
Recommends:
 libproxy1v5,
 libzypp-common (>= ${source:Version}),
 libzypp-config (>= ${source:Version}),
 libzypp-bin (>= ${source:Version}),
 logrotate,
 lsof,
Suggests:
 libzypp-doc (>= ${source:Version}),
Breaks:
 libzypp (<< 17.22),
 zypper (<< 1.14),
Replaces:
 libzypp (<< 17.22),
Description: openSUSE/SLES package management system (library)
 libzypp is the package management library that powers applications like
 YaST, zypper and the openSUSE/SLE implementation of PackageKit.
 .
 libzypp provides all the functionality for a package manager:
 .
   - an API for package repository management, supporting most common
     repository metadata formats and signed repositories
   - an API for solving packages, products, patterns and patches (installation,
     removal, update and distribution upgrade operations) dependencies, with
     additional features like locking
   - an API for comitting the transaction to the system over a rpm target;
     supporting deltarpm calculation, media changing and installation order
     calculation
   - an API for browsing available and installed software, with some facilities
     for programs with an user interface
   - a suite of maintained solving testcases representing common and uncommon
     operations on Linux software management
 .
 This package contains the main shared library for the zypper package
 management system.

Package: libzypp-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librpm-dev (>= 4.4),
 libboost-dev,
 libboost-program-options-dev,
 libboost-test-dev,
 libboost-thread-dev,
 libgpgme-dev,
 libssl-dev,
 libcurl4-openssl-dev (>= 7.19.4),
 libudev-dev,
 libxml2-dev,
 libsolv-dev (>= 0.7.16-2~),
 libexpat-dev,
 libpopt-dev,
 libproxy-dev,
 libzypp1735 (= ${binary:Version}),
Description: openSUSE/SLES package management system library (development files)
 libzypp is the package management library that powers applications like
 YaST, zypper and the openSUSE/SLE implementation of PackageKit.
 .
 libzypp provides all the functionality for a package manager:
 .
   - an API for package repository management, supporting most common
     repository metadata formats and signed repositories
   - an API for solving packages, products, patterns and patches (installation,
     removal, update and distribution upgrade operations) dependencies, with
     additional features like locking
   - an API for comitting the transaction to the system over a rpm target;
     supporting deltarpm calculation, media changing and installation order
     calculation
   - an API for browsing available and installed software, with some facilities
     for programs with an user interface
   - a suite of maintained solving testcases representing common and uncommon
     operations on Linux software management
 .
 This package contains the development files for the libzypp library.

Package: libzypp-common
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: openSUSE/SLES package management system library (common files)
 libzypp is the package management library that powers applications like
 YaST, zypper and the openSUSE/SLE implementation of PackageKit.
 .
 libzypp provides all the functionality for a package manager:
 .
   - an API for package repository management, supporting most common
     repository metadata formats and signed repositories
   - an API for solving packages, products, patterns and patches (installation,
     removal, update and distribution upgrade operations) dependencies, with
     additional features like locking
   - an API for comitting the transaction to the system over a rpm target;
     supporting deltarpm calculation, media changing and installation order
     calculation
   - an API for browsing available and installed software, with some facilities
     for programs with an user interface
   - a suite of maintained solving testcases representing common and uncommon
     operations on Linux software management
 .
 This package contains the architecture-independent files for the libzypp
 library.

Package: libzypp-bin
Architecture: any
Multi-Arch: foreign
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libzypp-config (>= ${source:Version}),
Description: openSUSE/SLES package management system library (library tools)
 libzypp is the package management library that powers applications like
 YaST, zypper and the openSUSE/SLE implementation of PackageKit.
 .
 libzypp provides all the functionality for a package manager:
 .
   - an API for package repository management, supporting most common
     repository metadata formats and signed repositories
   - an API for solving packages, products, patterns and patches (installation,
     removal, update and distribution upgrade operations) dependencies, with
     additional features like locking
   - an API for comitting the transaction to the system over a rpm target;
     supporting deltarpm calculation, media changing and installation order
     calculation
   - an API for browsing available and installed software, with some facilities
     for programs with an user interface
   - a suite of maintained solving testcases representing common and uncommon
     operations on Linux software management
 .
 This package contains command line tools shipped with the libzypp library.

Package: libzypp-config
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 libzypp-common (>= ${source:Version}),
Description: openSUSE/SLES package management system library (configuration)
 libzypp is the package management library that powers applications like
 YaST, zypper and the openSUSE/SLE implementation of PackageKit.
 .
 libzypp provides all the functionality for a package manager:
 .
   - an API for package repository management, supporting most common
     repository metadata formats and signed repositories
   - an API for solving packages, products, patterns and patches (installation,
     removal, update and distribution upgrade operations) dependencies, with
     additional features like locking
   - an API for comitting the transaction to the system over a rpm target;
     supporting deltarpm calculation, media changing and installation order
     calculation
   - an API for browsing available and installed software, with some facilities
     for programs with an user interface
   - a suite of maintained solving testcases representing common and uncommon
     operations on Linux software management
 .
 This package ships the configuration files for the libzypp library.

Package: libzypp-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Build-Profiles: <!nodoc>
Depends:
 ${misc:Depends},
 libjs-jquery,
Suggests:
 libzypp1735,
 libzypp-dev,
Description: openSUSE/SLES package management system library (documentation)
 libzypp is the package management library that powers applications like
 YaST, zypper and the openSUSE/SLE implementation of PackageKit.
 .
 libzypp provides all the functionality for a package manager:
 .
   - an API for package repository management, supporting most common
     repository metadata formats and signed repositories
   - an API for solving packages, products, patterns and patches (installation,
     removal, update and distribution upgrade operations) dependencies, with
     additional features like locking
   - an API for comitting the transaction to the system over a rpm target;
     supporting deltarpm calculation, media changing and installation order
     calculation
   - an API for browsing available and installed software, with some facilities
     for programs with an user interface
   - a suite of maintained solving testcases representing common and uncommon
     operations on Linux software management
 .
 This package contains the API documentation of the libzypp library.
